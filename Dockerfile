FROM maven:3.6.1-jdk-11-slim

WORKDIR /app
COPY target/dog-1.0-jar-with-dependencies.jar ./target/dog-1.0-jar-with-dependencies.jar
COPY run.sh .

ENTRYPOINT ["./run.sh"]
CMD ["2"]
